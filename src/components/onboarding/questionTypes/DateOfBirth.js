import React from 'react';

import { Picker } from '@react-native-picker/picker';
import PropTypes from 'prop-types';
import { Platform, StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import AnswerTemplate from './AnswerTemplate';

import testIDs from '../../../../e2e/modulesTestIDs';
import theme from '../../../config/theme';
import { getDays, getMonths, getYears } from '../../../helpers/birthDayHelper';
import { checkDisabled } from '../../../helpers/questionAnswerHelpers';
import normalizeValue from '../../../helpers/normalizeValue';

function DateOfBirth({
	currentTask = {},
	doUpdateTask = () => {},
	state = {},
	setState = () => {},
}) {
	const isIos = Platform.OS === 'ios';
	const nextButtonDisabled = checkDisabled(currentTask, state);

	let pickers = {};

	const dayPlaceholder 	= "Dag";
	const monthPlaceholder 	= "Maand";
	const yearPlaceholder 	= "Jaar";
	const months 			= getMonths();

	const getMonthLabel = (month) => {
		month -= 1;
		if (months[month] === undefined)
			return monthPlaceholder;
		return months[month].label;
	}

	return (
		<AnswerTemplate
			currentTask={currentTask}
			nextButtonDisabled={nextButtonDisabled}
			doUpdateTask={() => doUpdateTask()}
		>
			<View style={[styles.container, !isIos && styles.containerAndroid]}>
				<View style={[styles.pickerContainer, !isIos && styles.pickerAndroid]}>
					<Picker
						ref={item => pickers.day = item}
						testID={testIDs.QUESTION.PICKER_DAY}
						selectedValue={state.day}
						onValueChange={(itemValue) =>
							setState({
								...state,
								day: itemValue,
							})
						}
					>
						<Picker.Item label={dayPlaceholder} value="" />
						{getDays()}
					</Picker>
				</View>

				<View style={[styles.pickerContainer, !isIos && styles.pickerAndroid]}>
					<Picker
						ref={item => pickers.month = item}
						testID={testIDs.QUESTION.PICKER_MONTH}
						selectedValue={state.month}
						onValueChange={(itemValue) =>
							setState({
								...state,
								month: itemValue,
							})
						}
					>
						<Picker.Item label={monthPlaceholder} value="" />
						{months.map((month) => (
							<Picker.Item label={month.label} value={month.value} key={month} />
						))}
					</Picker>
				</View>

				<View style={[styles.pickerContainer, !isIos && styles.pickerAndroid]}>
					<Picker
						ref={item => pickers.year = item}
						testID={testIDs.QUESTION.PICKER_YEAR}
						selectedValue={state.year}
						onValueChange={(itemValue) =>
							setState({
								...state,
								year: itemValue,
							})
						}
					>
						<Picker.Item label={yearPlaceholder} value="" />
						{getYears()}
					</Picker>
				</View>
			</View>

			{/* Android picker delegates.*/}
			{!isIos &&
				<View style={styles.pickerDelegateContainer}>
					<TouchableOpacity style={styles.pickerDelegate} onPress={() => {pickers.day.focus()}}>
						<Text style={[styles.pickerDelegateText, state.day != "" && styles.pickerDelegateTextConcrete]}>
							{state.day == "" ? dayPlaceholder : state.day}
						</Text>
					</TouchableOpacity>

					<TouchableOpacity style={styles.pickerDelegate} onPress={() => {pickers.month.focus()}}>
						<Text style={[styles.pickerDelegateText, state.month != "" && styles.pickerDelegateTextConcrete]}>
							{getMonthLabel(state.month)}
						</Text>
					</TouchableOpacity>

					<TouchableOpacity style={styles.pickerDelegate} onPress={() => {pickers.year.focus()}}>
						<Text style={[styles.pickerDelegateText, state.year != "" && styles.pickerDelegateTextConcrete]}>
							{state.year == "" ? yearPlaceholder : state.year}
						</Text>
					</TouchableOpacity>
				</View>
			}
				
		</AnswerTemplate>
	);
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignSelf: 'stretch',
	},
	containerAndroid: {
		height: 0,
		overflow: "hidden",
	},
	pickerContainer: { flex: 1 },
	pickerAndroid: {
		borderWidth: 1,
		borderRadius: 5,
		marginRight: theme.spacing.xxs,
	},
	pickerDelegateContainer: {
		display: "flex", 
		flexDirection: "row", 
		width: "100%",
	},
	pickerDelegate: {
		borderWidth: 1,
		borderRadius: 5,
		paddingTop: normalizeValue(15),
		paddingBottom: normalizeValue(15),
		display: "flex",
		flex: 1,
		marginRight: theme.spacing.xxs,
		width: "100%",
	},
	pickerDelegateText: {
		fontSize: normalizeValue(16),
		width: "100%",
		textAlign: "center",
		color: theme.colors.greyedOut,
	},
	pickerDelegateTextConcrete: {
		color: theme.colors.black,
	},
	picker: {
		height: 10,
	},
});

DateOfBirth.propTypes = {
	currentTask: PropTypes.object.isRequired,
	doUpdateTask: PropTypes.func.isRequired,
	state: PropTypes.object.isRequired,
	setState: PropTypes.func.isRequired,
};

export default DateOfBirth;
